\chapter{Evaluation}
\label{evaluation}

% Evaluation
%	metrics
%   benchmarks

%�

In this section, the implementation of the different approaches will be tested with a set of benchmark applications. The results show the performance difference in comparison to a test run without allocation stack trace sampling as well as the accuracy of the sampling results for different sampling methods.



\section{Test Setup}
\label{test-setup}

All tests were run on machines with the Ubuntu 17.04 operating system (kernel version: GNU/Linux 4.10.0-32-generic x86\_64). The used Java runtime environment was OpenJDK 8u122. Older versions could not be used because of the bug mentioned in Section \ref{cct-flushing}. The machine used for the performance tests has an Intel(R) Core(TM) i7-4790K CPU @ 4.00GHz while the machine used for the accuracy tests has an Intel(R) Core(TM) i7-3770 CPU @ 3.40GHz, otherwise the hardware and software was the same. Both machines have 32 GB of RAM installed. Dynamic frequency scaling and turbo boost have been disabled to get more accurate and more stable results.

\subsection{Benchmarks}
% benchmarks: DaCapo, DaCapoScala; SPECjbb/SPECjvm not suitable for accuracy evaluation (non-constant workload); eclipse tomcat tradebeans tradesoap Java 8 problems; size large except unsupported or problems (eclipse)

For testing the implementation under real-world conditions, benchmark applications from the DaCapo benchmark suite 9.12 \cite{Blackburn2006}, from the Scala benchmark project 0.1.0 \cite{Sewe2011} as well as from the SPECjvm2008 benchmark suite 1.01\footnote{\url{https://www.spec.org/jvm2008/}} have been used. Due to a known JVM bug\footnote{\url{https://bugs.openjdk.java.net/browse/JDK-8169330}} several benchmark applications crashed frequently and thus had to be disabled because an insufficient amount of test results was generated. The benchmark applications \textit{compiler.compiler} (SPECjvm) and \textit{kiama}, \textit{scalac}, \textit{scaladoc}, \textit{scalatest}, \textit{scalaxb} (DaCapoScala) are affected by this problem. Additionally, the benchmark applications \textit{jython}, \textit{tomcat}, \textit{tradebeans} and \textit{tradesoap} (DaCapo) had to be disabled because they do not work correctly on the used JDK version.

\subsection{Test Configurations}

Multiple configurations have been tested for both the allocation-based as well as the time-based methods. A basic configuration option for all test runs is the \textit{sampling interval}. For allocation-based methods this specifies the number of allocation counter increments between two stack traces samples. For time-based methods it specifies the time between two stack trace samples in milliseconds. The \textit{sampling rate} or \textit{sampling frequency} is the reciprocal of the sampling interval. For time-based sampling methods an option allows us to enable the usage of BCI data (allocation probabilities). \\
A performance optimization option allows us to limit the stack trace sample to a fixed number of stack frames. Further stack frames are ignored and not even retrieved, which may improve the run-time overhead. The topmost stack frames (representing the current method and its parents) are kept.

In summary the following combinations of options have been used:
\begin{itemize}[nosep]
	\item allocation-based sampling methods
	\begin{itemize}[nosep]
		\item a sample every 100 allocations
		\item a sample every 1000 allocations
		\item a sample every 10000 allocations
		\item a sample every 1000 allocations with the stack depth limited to 5 entries
		\item a sample every 1000 allocations with the stack depth limited to 8 entries
	\end{itemize}
	\item time-based sampling methods
	\begin{itemize}[nosep]
		\item a sample every 5ms
		\item a sample every 10ms
		\item a sample every 30ms
		\item a sample every 10ms with BCI data usage enabled
	\end{itemize}
\end{itemize}



\section{Comparison of Calling Context Trees}
% - CCT UI / visualization / comparison
% - how to handle own allocations, observer effect

For evaluating the accuracy of the allocation assignment results the most obvious approach is to compare two data sets: one with known correct data and the tool's output as the other one. This comparison to a ground truth could then give some hints about the validity of the sampled results. The ground truth should match in terms of the call hierarchy and the number, type, location and context of allocations.

However, getting a ground truth might not be easily possible due to some of the following reasons (among others):
\begin{itemize}[nosep]
	\item threads: the application might run a variable number of threads depending on some arbitrary decision. In that case the tasks might be assigned differently to the existing threads. That may even happen when the number of threads is the same because of some scheduling strategy of the application or the operating system. This may cause allocations being executed in a different thread than intended. It does not affect the overall number of allocations of all threads, though.
	\item randomness: the application might use heuristic algorithms based on random events or values. This might alter the code execution so that some part is executed more or less often than intended. If that code performs allocations the number of allocations would vary. This would also affect the overall number of allocations of all threads.
	\item observer effect: the application might behave differently when the DuckTracks CCT agent is attached and retrieves stack traces periodically. The effect would be similar to the previous issue.
\end{itemize}


\subsection{Retrieving a Ground Truth}
\label{ground-truth}
There are basically two ways how a ground truth can be determined: either in the same test run where the sampling results are collected or in a separate run of the test application. When fetching the data for the ground truth at the same time as the sampling is done several of the issues discussed above are avoided. As both data sets are collected in the same test run we can assure that the application under test behaved in the same way for both data collections. On the other hand, the additional data collection might interfere with the other sampling-based data collection. For example, when using the time-based sampling approach some samples might be retrieved while the ground truth data collector is active. This would falsify the result of the sampling approach. This problem does not occur for the allocation-based sampling approach.

A ground truth retrieved externally in a separate run of the test/benchmark application could suffer from all the issues listed in the previous section. In that case, choosing appropriate test applications is important. Especially the use of multi-threading (number of threads, assignment of tasks to threads, inter-thread dependencies) and other factors influencing the predictability of a test run should be taken into account.

The ground truth can be retrieved by getting a full stack trace for every allocation counter increment. A slight variation of the allocation-based sampling algorithm already allows us to do this: all that needs to be done is setting the sampling frequency to one (or lower) so that a sample is taken at every allocation. The final counter assignment can be disabled. The actual implementation builds a separate calling context tree to allow using sampling rate one and a higher sampling rate at the same time.




\subsection{Correction factors}
\label{correction-factors}
%   - correction factors (different threads, execution strategies, ...)
%   - total allocation count correction

Instead of trying to get an accurate ground truth for a certain sampled result one can also try to apply corrections to the sampled result. That has to be done in a way that allows a comparison but does not modify relevant properties because we do not want to massage the facts. Such an important property to evaluate the allocation assignment is the distribution ratio of the allocations of a certain method. The total number of allocations is not that important on the other hand because that data is retrieved from DuckTracks and is known to be correct anyway.

Thus a possible correction is to equalize the total number of allocations for each allocated data type while preserving the distribution ratio between different CCT nodes related to such an allocation. This is done by calculating a correction factor $ f = \frac{reference\_total\_allocs}{actual\_total\_allocs} $ and multiplying all the allocation counts of the according data type by that factor. For example consider two methods \textit{A} and \text{B} both allocating an object \textit{X}. Further assume that allocation sampling gave the following results: $9X$ in method $A$, $18X$ in method $B$. The reference results for this example are: $10X$ in method $A$ and $20X$ in method $B$. One can see that the distribution ratio between the two methods represented as CCT nodes and related to type \textit{X} is $9:18$ resp. $10:20$, thus equal. To apply the correction described above, multiply both $9X$ and $18X$ with the factor $\frac{10+20}{9+18}$.




\section{Result Accuracy}

The data set for the accuracy evaluation has been retrieved by executing a single benchmark iteration only. The DaCapo and DaCapoScala benchmark suites were used for these tests. The SPECjvm benchmark suite is not part of the accuracy evaluation because it uses a constant time rather than a constant workload per iteration and thus does not allow us to compare the CCTs of independent test runs. The reference data is collected with the sampled data (in the same test run) for allocation-based sampling and separately (in an independent test run) for time-based sampling (see Section \ref{ground-truth}). A correction of the total number of allocations is applied for the results of time-based sampling as described previously.

\subsection{Metric}

A metric describing the result's accuracy should rate the accuracy precisely, should be easy to interpret and should include only relevant data. The chosen metric is the number of incorrectly assigned allocations in relation to the total number of allocations. For example, if a CCT node has the allocation of 80 \texttt{Integer}s assigned while there should be 100, the total allocation count is increased by 100 and the total error count is increased by 20. Since errors would be counted twice that way (at the location where allocations are missing and at the location to which they were wrongly assigned) the error count is divided by two.


\subsection{Allocation-based sampling}

Figure \ref{fig:graph-accuracy-asample} shows the error rate for calling context trees with allocation information built by allocation-based sampling approaches. The tests have been executed with the configurations described previously. The reference data for the tests with limited stack trace depth is also limited to the same stack trace depth.

In general, the result is better (or - in other words - the error rate is lower) when the sampling frequency is higher. The total number of allocations that are performed by a benchmark application considerably influences the results. For example, the results for \textit{batik} (few allocations) are bad while the ones for \textit{factorie} (many allocations) are excellent. Reducing the number of nodes in the CCT also improves the results, but also reduces the total amount of available information on the other hand. For example, while the error rate is low for the CCT limited to a depth of 5 there is only information about maximal 5 callers available.


\bildTW{Gfx/graph-accuracy-asample.png}{Ratio of incorrectly assigned allocations in a CCT built with allocation-based sampling data}{fig:graph-accuracy-asample}



\subsection{Time-based sampling}

Figure \ref{fig:graph-accuracy-sample} shows the error rate for CCTs with allocation information built by time-based sampling approaches. Test configurations with a sample rate of 5ms, 10ms and 30ms and one with CCT hinting enabled have been executed. 

Higher sampling frequencies imply a better accuracy most of the time, but the results are rather bad overall with error rates between 20 and 30 percent. The use of byte code index data does only improve the results for about half of the benchmarks. For the others, the results are in some cases significantly worse. This seems to confirm our concerns mentioned in Section \ref{sampling_issues}.


\bildTW{Gfx/graph-accuracy-sample.png}{Ratio of incorrectly assigned allocations in a CCT built with time-based sampling data}{fig:graph-accuracy-sample}


\clearpage   % room to put the two figures above on one page

\section{Run Time}

The performance our techniques has been measured by recording the run time of a single test iteration after 20 warm-up test iterations. The value therefore represents the peak performance of a single test run. This procedure has been repeated 5 times. The performance value equals the arithmetic mean of the test run results in relation to an average DuckTracks test run without CCT extensions.

\subsection{Allocation-based sampling}

Figure \ref{fig:graph-runtime-asample-dacapo} and Figure \ref{fig:graph-runtime-asample-specjvm} show the run-time overhead as described above for the DaCapo/DaCapoScala and SPECjvm benchmark suites, respectively. Multiple configurations of the allocation-based sampling algorithm have been tested as described in the \nameref{test-setup} section.

As expected, the overhead rises with the number of samples and with the number of allocations in a benchmark application. Lengauer et al. evaluated allocation metrics for the benchmark applications including the number of allocations per test iteration and per second \cite{Lengauer2017}. The benchmarks with a large number of allocations (e.g. \textit{tmt}, \textit{factorie} (DaCapoScala) and \textit{derby}, \textit{serial}, \textit{sunflow} (SPECjvm)) correspond to the peaks in the figures. A sampling rate of 1 sample per 100 allocations adds a considerable run-time overhead. The overhead is greatly reduced by lowering the sampling frequency. Limiting the number of entries in the stack trace samples further improves the performance. Choosing a sampling rate of 1 sample per 1000 allocations and a stack depth limit of 5 entries results in a run-time overhead less than 50\% in most of the benchmarks.


\bildTW{Gfx/graph-runtime-asample-dacapo.png}{Run time of DaCapo/DaCapoScala benchmarks with allocation-based sampling relative to DuckTracks without CCT sampling}{fig:graph-runtime-asample-dacapo}
\bildTW{Gfx/graph-runtime-asample-specjvm.png}{Run time of SPECjvm benchmarks with allocation-based sampling relative to DuckTracks without CCT sampling}{fig:graph-runtime-asample-specjvm}


\subsection{Time-based sampling}

Figure \ref{fig:graph-runtime-sample-dacapo} and Figure \ref{fig:graph-runtime-sample-specjvm} show the run-time overhead of the time-based sampling approach for the DaCapo/DaCapoScala and SPECjvm benchmark suites, respectively. The test configurations for time-based sampling methods were used as described before.

In contrast to the allocation-based sampling method, the allocation characteristics of a benchmark application do not influence the run-time overhead that much, but there is still a slightly increased overhead visible for benchmark applications with a large number of allocations. A reduction of the sampling frequency reduces the run-time overhead as expected. CCT hinting reduces performance considerably, for a few benchmarks even by a factor of two or higher. 


\bildTW{Gfx/graph-runtime-sample-dacapo.png}{Run time of DaCapo/DaCapoScala benchmarks with time-based sampling relative to DuckTracks without CCT sampling}{fig:graph-runtime-sample-dacapo}
\bildTW{Gfx/graph-runtime-sample-specjvm.png}{Run time of SPECjvm benchmarks with time-based sampling relative to DuckTracks without CCT sampling}{fig:graph-runtime-sample-specjvm}




