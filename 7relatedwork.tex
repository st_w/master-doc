\chapter{Related Work}
\label{relatedwork}

%�

\section{Existing Tools}
There are many implementations of Java memory monitoring solutions, which are already widely used by Java developers. The following paragraphs will describe some common tools in general and highlight commonalities and differences in their memory monitoring approach compared to the DuckTracks-based solution discussed in this thesis.


\begin{description}
	% https://www.yourkit.com/docs/java/help/allocations.jsp
	\item[YourKit Java Profiler]\footnote{\url{https://www.yourkit.com/java/profiler/}} is a proprietary, commercial solution for profiling CPU, memory, thread or I/O usage among others. Particularly for memory profiling it provides several object allocation monitoring methods including a mode where only the number and type of the allocations are recorded and another one that additionally records the thread and the methods where the allocations happened. The former mode gives the same results as DuckTracks. The latter records calling context data for each allocation, which results in a big overhead. However, the YourKit Java Profiler offers some options to reduce the overhead. One option is to record only object allocations matching some criteria such as the object size. For example, small objects can be sampled while big objects are recorded with full calling context information. Another option is to estimate the calling contexts instead of using exact data by using a sampling approach.
	
	The sampling method of the YourKit Java Profiler works in a pretty simple way: all recorded allocations are assigned to the most recently sampled stack trace until a new one is retrieved with the next sample, according to the documentation \cite{YourKit2013}. This is a fundamentally different approach than the ones presented in this thesis. It will result in a correct assignment of stack traces to allocations as long as the allocating method's execution time is longer than the sampling period. However, the accuracy of YourKit for realistic sampling rates has not been described by its developers. The tool's documentation also mentions that expert knowledge might be required to interpret the results due to the used sampling technique.
	
	
	% https://plumbr.eu/handbook/gc-tuning-measuring/profilers/java-visualvm
	\item[Java VisualVM] is a free open source\footnote{\url{https://visualvm.github.io/}} tool for monitoring Java applications developed by Oracle. Among a performance/resource monitor and a thread activity visualization with deadlock detection it also features a CPU/memory sampler and a profiler. The difference between sampling and profiling in VisualVM is their respective method to get CPU or memory data: the former uses a sampling approach while the latter uses an instrumentation-based approach. Additional features can be added by installing one of the various plugins.
	
	VisualVM supports analyzing applications while they are running and displays live statistics and monitoring results. It can connect to local or remote processes. Additionally, it allows users to analyze VM core dump files and application snapshots. In contrast to DuckTracks, which performs the code instrumentation as soon as a class is loaded for the first time, VisualVM performs the instrumentation only when profiling is enabled by reloading all the classes at that point. After disabling profiling, the classes are reloaded again removing the previous instrumentation.
	
	In the memory sampling mode VisualVM continuously fetches the currently living objects in memory. The sampling rate can be configured and is set to 100ms by default. Using snapshots, the user can save the state at a certain point in time and can compare such snapshots afterwards. However, VisualVM does not incorporate any information about memory allocations or allocation stack traces and thus is not really comparable to the memory monitoring solution in this thesis.
	
	Finally, the profiler mode of VisualVM records memory allocation information and optionally also garbage collection and allocation stack trace information. By default, that data is retrieved for every 10th object  allocation, but the interval is configurable. The data is retrieved using instrumentation and the profiler can be dynamically attached or detached at any time as mentioned above. When garbage collection data is available VisualVM shows the number and size of the objects being alive, otherwise it reports the number and size of the allocated objects just like DuckTracks. To view allocation stack traces one has to take a snapshot of the current results first. VisualVM shows the absolute number of allocated objects (since profiling was started) and uses the number of samples as allocation count for the stack trace view \cite{Oracle2016VisualVM}. It does not seem to estimate actual allocation numbers. Neither does it provide results separated by threads (however, the sampling mode implements that feature).
	
	
	
	% http://hirt.se/blog/?p=381
	% http://blog.takipi.com/oracle-java-mission-control-the-ultimate-guide/
	% http://www.eclipse.org/community/eclipse_newsletter/2014/january/article2.php
	% http://hirt.se/blog/?p=364
	% https://www.infoq.com/news/2013/10/misson-control-flight-recorder
	% http://hirt.se/blog/?p=277
	\item[Java Mission Control] was developed by Oracle and implements similar functionality as other memory monitoring and profiling solutions such as VisualVM. It is a commercial, proprietary product but distributed freely along with the JDK for evaluation purposes. Unlike the previous tools described above, Mission Control uses a different approach for data acquisition that is not based on sampling or instrumentation, but rather on events generated by a modified JVM. These special \textit{flight recorder} events can be emitted by the HotSpot JVM and are recorded by the \textit{Flight Recorder}, the recording component of Mission Control \cite{Lange2013}. Benefits of that approach include the lightweight and fast recording and the ability to run the existing bytecode unmodified, preventing possible side-effects. On the other hand, a big downside is that the technique requires a special JVM implementation providing those flight recorder events.
	
	Similar to VisualVM, Mission Control can connect to local or remote Java processes for detailed analysis. After connecting to a process, Flight Recorder can be instructed to record profiling data. It provides two different operation modes: in time-fixed mode, data is recorded for a fixed timespan only and in continuous mode data is recorded continuously. The types of events to be monitored can be selected.
	
	Allocation monitoring results are categorized in thread-local allocations, which are allocated in the Thread Local Allocation Buffer (TLAB), and other allocations that are allocated outside a TLAB. For each of those categories Mission Control shows the type and the size of the allocated objects as well as stack traces for each allocation. In contrast to the monitoring solution discussed in this thesis the tool allows users to specify a time slot to analyze allocations inside that time slot only.
	
	
	
	\item[AntTracks] is a free open source\footnote{\url{http://mevss.jku.at/?page_id=1592}} memory monitoring solution described and originally implemented by Lengauer et al. \cite{Lengauer2015}. It is capable of acquiring detailed information about the lifetime of Java objects. The tool uses an event-based approach to get data about memory allocations and garbage collector activity, similar to the one used by Java Mission Control. The event emitting code is implemented as a patch for the Java Hotspot VM, version 8. A VM build including that patch, called \textit{AntTracks VM}, is required to use the tool.
	
	Compared to Mission Control AntTracks collects more detailed data about the object allocations and garbage collections. While Mission Control only offers statistics for data types and allocation sites AntTracks is able to provide exact data about any object (de-)allocated in the monitoring period. This data includes the object's type, size, allocation site and allocating thread, among others. 
	%For some object types it even provides the data contents. For example, full pointer information can be recorded with minimal overhead \cite{Lengauer2016}. 
	With the collected data it is possible to restore the state of the heap at any time in the monitoring period. The tool relies on known internal behavior of garbage collectors and the object allocation algorithms to regenerate as much data as possible and thus minimize the amount of data that needs to be recorded at monitoring time.
	
	The included visualization tool allows users to compare any two states of the heap, showing exactly the object instances that were allocated or deallocated. Additionally a set of graphs is provided, illustrating the garbage collection activity, memory consumption or object kinds. A user-definable filtering and grouping mechanism gives the user a powerful tool for customizing the visualization.
	
	
	% (C/C++)
	% http://www.deleaker.com/docs/user_interface/allocations.html
	% http://valgrind.org/docs/manual/ms-manual.html
	
	
\end{description}







\section{Future Work}
% http://psy-lob-saw.blogspot.com/2017/02/flamegraphs-intro-fire-for-everyone.html


The processing of the calling context tree in Java caused some problems that could largely be avoided by implementing the processing in native code (like C/C++). For example, numerous Java allocations performed in the processing code could be avoided by using C-style memory allocations with \texttt{malloc}, which would not trigger any sampling or interfere in other ways with the allocation sampling. Thus, the logic for preventing recursive sampling calls or to disable sampling temporarily would not be necessary. However, a complete C implementation would require us to implement bytecode instrumentation in native code, too.

Currently a very simple approach is used for dealing with loops when calculating allocation probabilities for certain bytecode index ranges. A more sophisticated algorithm could detect loops in a separate step and incorporate loop paths in the path probability calculation instead of stopping processing on the first occurrence of a duplicate node. This could improve the accuracy of the time-based sampling approach with allocation probabilities. On the other hand, it would cause an even higher run-time performance overhead for that approach.

The visualization tool presented in Section \ref{user-interface} offers quite limited functionality. A big advantage would be to allow customizing the filtering, grouping and ordering of the CCT data instead of providing a fixed set of views. A general approach for such a system has been presented by Weninger \cite{Weninger2017a}. This approach has already been implemented for the AntTracks UI and could probably be adapted for DuckTracks CCT viewer, too.


