CONSTANTS
	intConstant: INT := 1234;
	stringConstant: STRING := "teststring";
	booleanConstant: BOOLEAN := TRUE;
END

TYPES
	Fontvieille = STRUCT
		moneghetti: STRING;
	END 
END

// Hier wird das Interface "Codamine" eingef�hrt
INTERFACE Codamine
	ROUTINE MonacoVille(monaco:Fontvieille);
END Codamine

// Die Komponente "Larvotto" implementiert das Interface "Codamine"
COMPONENT Larvotto IMPLEMENTS Codamine
	PARAMETERS
		colle:Fontvieille;
		
	// Diese Funktion ist �ber das Interface nicht erreichbar
	ROUTINE SaintRoman()
	BEGIN
		MonacoVille(colle);		
	END
	// Diese Funktion implementiert das Interface "Codamine"
	ROUTINE MonacoVille(monaco:Fontvieille)
	BEGIN
		MSG monaco.moneghetti;
	END
END Larvotto

SETUP
VARS
	// Hier wird eine Instanz der Komponente "Larvotto" angelegt
	michel: Larvotto;
BEGIN
	michel.colle.moneghetti := "R�voires";
	
	// Das Programm beginnt mit der Funktion "SaintRoman"
	michel.SaintRoman();
END SETUP
