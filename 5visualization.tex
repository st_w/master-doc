\chapter{Visualization}
\label{visualization}

%�

The result of running an application with the DuckTracks agent (extended with CCT sampling) attached is a set of text files. These contain the calling context trees for all the threads along with the allocations assigned to certain nodes in the CCT. For a user this representation is pretty unreadable as the file easily gets very big so that extracting the essential information is practically impossible. This chapter describes different representations of the CCT data and a tool developed for browsing and analyzing that data.


%   
\section{Calling Context Tree Representations}
%   - inverse cct, grouped by types, how to build it
%   - summarizing bci cct

The default representation of a CCT as used by the monitoring application is a top-down view of the tree. The \texttt{main} method is on top and its callees are below. This structure is also used by the text files emitted as result of the memory allocation monitoring. While this representation is easy to build it is not optimal from a user's perspective because it requires diving into the tree to get any relevant information. The top levels contain the main methods and are not very interesting.


\subsection{Inverse Calling Context Tree}

A common transformation is to invert the calling context tree, building a bottom-up view. This transformation puts the methods, which previously were represented as leafs, on top and the callers beneath them. That way, the user can easily get an overview of all the methods and sees the immediate callers for each method by navigating to the next level in the tree. The deeper the tree gets the more detailed is the available calling context data for a certain method. Limiting the depth to a certain value still leaves useful data, while doing the same with a non-inverted tree would render the data practically useless as many allocations would be lost.

\bildS{Gfx/inverse-CCT.pdf}{Different representations of a calling context tree: top-down and bottom-up (inverse)}{fig:inverse-cct}{scale=0.6}

A sample CCT along with its inverse representation is shown in Figure \ref{fig:inverse-cct}. It contains four methods: \textit{m(ain)}, \textit{A}, \textit{B} and \textit{C} along with some invocation counter values. To build an inverse CCT from a CCT we start with iterating the tree in any order and pick the nodes with allocations to add them beneath a new \textit{root} node. In the example this will add all nodes at the first level beneath the new root node. When one creates the children of those nodes by using the \textit{parent} reference instead of the \textit{children} list one gets a tree already very similar to the one shown in the example on the right-hand side. The only difference is that there are two \textit{B} nodes beneath the inverse CCT root. Thus in a final step the equal nodes have to be merged on all levels.


\subsection{Grouping}

Grouping allows us to summarize several paths of a calling context tree using custom rules. The probably most useful and most common grouping for CCTs with allocation data is by allocation type and is typically applied to an inverse CCT. The current implementation uses lists of tree paths for grouping. A \texttt{TreePath} is represented by the leaf node and a \textit{current} node, which stores the current level in the tree during processing. That allows us to directly use the intermediate result of the inverse CCT build process where the nodes have not been merged yet. Merging can be implemented by using \texttt{TreePathCollection}s, which subdivide a set of tree paths into groups by allocation type.

Several common grouping settings are illustrated in Figure \ref{fig:cct-grouping}. On the left-hand side the calling context tree and the according inverse CCT are shown without any grouping applied. The types of the allocated objects are represented as gray circles. There are no allocations in the method \textit{m} and thus it can be hidden in the inverted CCTs as illustrated by the use of dashed lines. The tree in the center of the figure is grouped by the allocated object's type. The tree on the right-hand side of the figure is first grouped by the allocating method and then by the allocated type.

\bildTW{Gfx/CCT-grouping.pdf}{Illustration of several common calling context tree grouping settings}{fig:cct-grouping}

\subsection{CCT with Byte Code Index Data}

Calling context trees with BCI data have a separate node for each different bytecode index as explained in Section \ref{extending-ccts-with-bci-data} in more detail. If the tree would be visualized as-is this would result in a very large amount of nodes. Thus the nodes representing the same method but different BCIs are merged before visualization. The result is a CCT that looks the same as if there were no BCI data at all from the beginning.




\section{User Interface for Analyzing and Comparison}
\label{user-interface}
% based on AntTracks UI
% ref Markus' paper?

Nearly any further analysis on the data output from DuckTracks with CCT extensions requires a tool to handle the large amount of data. Typical analysis tasks include browsing the calling context tree and comparing two different sampling results. For that purpose, a custom tool titled \textit{DuckTracks CCT viewer} has been written. It is implemented in Java using the \textit{Swing} UI framework. The code is based on \textit{AntTracks UI} \cite{Weninger2017a}, but many parts except the core have been removed or replaced.

The tool's primary task is to present calling context trees with allocation data so that the user can easily browse them. To do so, a folder containing the results of a test run has to be specified first. It is created when running an application with the DuckTracks CCT agent attached. Such a folder typically contains several text files - one for each executed thread. The CCT viewer will show a list with all the threads that were found in the selected folder. It allows the user to either analyze the data of a single thread or to merge the results from all threads. After the thread has been chosen the text files are parsed and the tree is rendered (see Figure \ref{fig:cctviewer-view}). The default view groups the allocations by allocation type and uses an inverted CCT. For each method in the tree the number of allocations (absolute numbers and relative to the total number of allocations) and the number of samples is shown.

Another important feature of the tool is its ability to compare two calling context trees. If the folder with the DuckTracks CCT results contains several results (e.g. one using time-based sampling and another one using allocation-based sampling) the tool automatically tries to match the threads based on their name and ID. Alternatively, a second folder can be selected in case the results are stored in different folders. When selecting a certain pair of threads, the corresponding CCT data files are parsed, merged and compared. In addition to a browsing view for each of the trees, several comparison views are available as well in this case. All three comparison views show an inverted CCT. The first one is grouped by allocation type (see Figure \ref{fig:cctviewer-compare}), the second one is not grouped and the third one is first grouped by method and then by allocation type on the second level. Those grouping settings are illustrated in Figure \ref{fig:cct-grouping}. In addition to the allocation count and the number of samples the absolute and relative error is shown.


\bildTW{Gfx/CCTViewer-view.png}{Screenshot of DuckTracks CCT viewer while browsing a CCT with allocation data}{fig:cctviewer-view}


\bildTW{Gfx/CCTViewer-compare.png}{Screenshot of DuckTracks CCT viewer displaying a comparison of two CCTs}{fig:cctviewer-compare}



